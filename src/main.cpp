/* The following program is used to alarm if the temperature limit is exceeded by the furnace */

#include <Arduino.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <LiquidCrystal_I2C.h>
#include <Wire.h>
#include <Timers.h>
#include <RotaryEncoder.h>
#include <OneButton.h>
#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <NTPtimeESP.h>

#define PHASE_A_ENCODER_PIN D1  // GPIO pin, to which the phase A output of the encoder is connected
#define PHASE_B_ENCODER_PIN D2  // GPIO pin, to which the phase B output of the encoder is connected
#define ENCODER_BUTTON D5       // GPIO pin, to which the encoder button is connected
#define ONE_WIRE_BUS D6         // GPIO pin for One Wire communication
#define BUZZER_PIN D7           // GPIO pin, to which the buzzer is connected
#define ON_OFF_NETWORK true    // turn on/off connection to network

OneWire oneWire(ONE_WIRE_BUS);                                                    // create a OneWire class object to start One Wire communication
DallasTemperature sensor(&oneWire);                                               // create a DallasTemperature class object with a reference to a OneWire class object
DeviceAddress sensorAddress = {0x28, 0x04, 0x8B, 0xED, 0x0B, 0x00, 0x00, 0x6C};   // address of DS18B20 temperature sensor
LiquidCrystal_I2C lcd(0x27, 16, 2);                                               // create a LiquidCrystal_I2C class object with the address of the LCD display
                                                                                  // and the number of its columns and rows
RotaryEncoder encoder(PHASE_A_ENCODER_PIN, PHASE_B_ENCODER_PIN, ENCODER_BUTTON);  // create a RotaryEncoder class object to handle the encoder
OneButton button(ENCODER_BUTTON, true);                                           // create a OneButton class object to handle multiple actions for one button
WiFiClient espClient;
PubSubClient client(espClient);
NTPtime NTPch("pl.pool.ntp.org"); // choose server pool as required
strDateTime dateTime;             // the structure into which the time taken from the NTP server is stored
        
/* #### OWN TIMERS #### */
Timer getTemperatureTimer;          // a timer to take the temperature from the sensor, every specified time
Timer refreshLCDTimer;              // timer for refreshing the LCD display, every specified time
Timer buzzerTimer;                  // timer needed to play back the alarm signal
Timer warningSoundDurationTimer;    // timer for setting the time after which the acoustic alarm signal for low temperature is to be switched off
Timer returnMainWindowTimer;        // timer for setting the time after which the low and high temperature setting screen should be switched off
Timer backlightLCDTimer;            // timer for setting the time after which the LCD backlight should be switched off
Timer sendTemperatureToBrokerTimer; // timer for sending temperature value to broker
/* #################### */

/* #### OWN VARIABLES #### */
const short int MIN_TEMP_BOTTOM = 20;
short int MIN_TEMPERATURE = 40;
const short int MIN_TEMP_UPPER = 50;
const short int MAX_TEMP_BOTTOM = 55;
short int MAX_TEMPERATURE = 65;
const short int MAX_TEMP_UPPER = 90;
const unsigned int WARNING_SOUND_TIME = 30;       // the time after which the audible alarm for low temperature is switched off (in seconds)
const unsigned int GETTING_TEMP_TIME = 1000;      // the time what temperature value is taken from the sensor (in miliseconds)
const unsigned int TURN_OFF_BACKLIGHT_TIME = 45;  // the time after which the LCD backlight switches off (in seconds)
const unsigned int RETURN_MAIN_WINDOW_TIME = 15;  // the time after which the display will return to the main window (in seconds)
const unsigned int SEND_TEMP_INTERVAL = 60;       // the time after which Wemos D1 mini sends temperature value to broker (in seconds)
const char* ssid = "Plus_490";                    // the SSID of the network to which Wemos D1 mini will be connected
const char* password = "zuzia123";                // password to this network
const char* mqtt_server = "broker.hivemq.com";    // the address of the broker to which the data will be sent
const unsigned int mqtt_port = 1883;              // port for communication with the broker
/* do not change the following variable values */
short int CURRENT_TEMPERATURE = 0;                                          // temperature value in degrees Celsius
unsigned char stateLCD = 0;
unsigned char stateAlarm = 0;
unsigned char stateButton = 0;
bool playSound = false;
uint8_t customDegreeChar[8] = {0xC, 0x12, 0x12, 0xC, 0x0, 0x0, 0x0, 0x0};   // own degree sign
uint8_t customMinTempChar[8] = {0x4, 0xA, 0xA, 0xA, 0xA, 0x11, 0x1F, 0xE};  // own low-temperature thermometer sign
uint8_t customMaxTempChar[8] = {0x4, 0xA, 0xE, 0xE, 0xE, 0x1F, 0x1F, 0xE};  // own high-temperature thermometer sign
int16_t positionEncoder = 0;
char msg[50];                                                               // table of signs to publish the temperature to the broker
byte actualHour = 12, actualMinute = 0, actualSecond = 0;                   // variables with the current time downloaded from the NTP server
/* ###################### */

/* #### DECLARATION OF OWN FUNCTIONS #### */
bool setupWiFi();
void reconnect();
bool checkWiFiConnection();
void sendDataViaMQTT(short int temperature);
void actualTime(); 
float getTemperature();
short int changeLimitTemperatures(short int temperature, short int bottomLimit, short int upperLimit);
void setLCDWindow(short int current_temp, short int min_temp, short int max_temp);
void printTextOnLcd(int temperature, String textRow2);
void printTextOnLcd(String textRow1, String textRow2, int thresholdTemperature);
void turnOffBacklightLCD(int seconds);
void turnOnBacklightLCD();
void setAlarmSound(short int current_temp, short int min_temp, short int max_temp, int warningInterval, int WarningFreaquency, int alarmInterval, int alarmFreaquency);
void playAlarmSound(int interval, int frequency);
void stopAlarmSound();
void myClickFunction();
void myDoubleClickFunction();
ICACHE_RAM_ATTR void encoderISR();
/* ##################################### */

void setup() {
  //Serial.begin(9600);
  pinMode(TX, FUNCTION_3);                                                          // change of mode for the GPIO TX pin to a mode that supports I2C protocol
  pinMode(RX, FUNCTION_3);                                                          // change of mode for the GPIO RX pin to a mode that supports I2C protocol
  Wire.begin(TX, RX);                                                               // start the transmission with I2C protocol on TX and RX pins
  pinMode(BUZZER_PIN, OUTPUT);                                                      // sets the pin to which the buzzer is connected as output
  attachInterrupt(digitalPinToInterrupt(PHASE_A_ENCODER_PIN), encoderISR, CHANGE);  // initiation of external interruptions on the pin to which the encoder phase A pin
                                                                                    // is connected
  lcd.init();                           // LCD display initialization
  lcd.backlight();                      // LCD backlight on
  lcd.createChar(0, customDegreeChar);  // save your own character in the memory of the LCD display
  lcd.createChar(1, customMinTempChar);
  lcd.createChar(2, customMaxTempChar);
  
  button.attachClick(myClickFunction);              // enable action handling for one button click
  button.attachDoubleClick(myDoubleClickFunction);  // enable action support for double-clicking
  button.setDebounceTicks(50);                      // set the debounce time (in milliseconds)

  getTemperatureTimer.begin(GETTING_TEMP_TIME);                 // set the sampling time for temperature measurement (in milliseconds)
  refreshLCDTimer.begin(20);                                    // set the LCD display refresh time by timer
  warningSoundDurationTimer.begin(SECS(WARNING_SOUND_TIME));    // set the low-temperature alarm activation time (in seconds)
  returnMainWindowTimer.begin(STOP);                            // set the time after which the temperature change screen will turn off
  backlightLCDTimer.begin(STOP);                                // set the timer time after which the LCD backlight switches off
  buzzerTimer.begin(STOP);
  sendTemperatureToBrokerTimer.begin(SECS(SEND_TEMP_INTERVAL)); // set the timer time after which Wemos D1 mini sends temperature value to broker

  if (ON_OFF_NETWORK) {
    if (setupWiFi() == true) {
      client.setServer(mqtt_server, mqtt_port);
    }
  }

  sensor.begin(); // initialization of DS18B20 temperature sensor
  encoder.begin();  // encoder initialization

  CURRENT_TEMPERATURE = (short int)getTemperature();

  lcd.clear();
}


void loop() {
  button.tick();

  if (ON_OFF_NETWORK) {
    if (checkWiFiConnection() == true) {
      actualTime();
      if (sendTemperatureToBrokerTimer.available() == true) {
        sendDataViaMQTT(CURRENT_TEMPERATURE);
        sendTemperatureToBrokerTimer.restart();
      }
    }
    else {
      actualHour = 12;
      actualMinute = 0;
      actualSecond = 0;
    }
  }

  if (getTemperatureTimer.available() == true) {
    CURRENT_TEMPERATURE = (short int)getTemperature();
  }
  
  if (stateLCD == 3) {
    MIN_TEMPERATURE = changeLimitTemperatures(MIN_TEMPERATURE, MIN_TEMP_BOTTOM, MIN_TEMP_UPPER);
  }
  else if (stateLCD == 4) {
    MAX_TEMPERATURE = changeLimitTemperatures(MAX_TEMPERATURE, MAX_TEMP_BOTTOM, MAX_TEMP_UPPER);
  }

  setLCDWindow(CURRENT_TEMPERATURE, MIN_TEMPERATURE, MAX_TEMPERATURE);
  setAlarmSound(CURRENT_TEMPERATURE, MIN_TEMPERATURE, MAX_TEMPERATURE, 1000, 4000, 500, 4000);
}

/* #### DEFINITIONS OF OWN FUNCTIONS #### */
bool setupWiFi() {
  lcd.setCursor(0, 0);
  lcd.print("Connecting to:");
  lcd.setCursor(0, 1);
  lcd.print(ssid);

  WiFi.begin(ssid, password);
  delay(1000);

  for (int i = 1; i <= 5; i++) {
    if (WiFi.status() != WL_CONNECTED) {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("WiFi failed");
      lcd.setCursor(0, 1);
      lcd.print("Attempt:");
      lcd.setCursor(9, 1);
      lcd.print(i);
      lcd.setCursor(10, 1);
      lcd.print(" of ");
      lcd.setCursor(15, 1);
      lcd.print(5);
      delay(1000);
      
      return 0;
    }
    else {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("WiFi connected!");
      lcd.setCursor(0, 1);
      lcd.print("IP:");
      lcd.setCursor(4, 1);
      lcd.print(WiFi.localIP());
      delay(1000);

      break;
    }
  }
  return 1;
}

void reconnect() {  // currently unused
  while (!client.connected()) {                   // loop until we're reconnected
    //Serial.print("Attempting MQTT connection...");
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);      // create a random client ID
    if (client.connect(clientId.c_str())) {       // attempt to connect
      //Serial.println("connected");
      client.publish("outTopic", "hello world");  // once connected, publish an announcement...
      client.subscribe("inTopic");                // ... and resubscribe
    } 
    else {
      /*Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");*/
      delay(5000);                                // wait 5 seconds before retrying
    }
  }
}

bool checkWiFiConnection() {
  if (WiFi.status() != WL_CONNECTED) {    // check WiFi connection
    return 0;
  }
  else if (client.connected() == false) { // check MQTT connection
    return 0;
  }
  else {
    return 1;
  }
}

void sendDataViaMQTT(short int temperature) {
  //client.loop();
  snprintf(msg, 50, "Time: %i:%i:%i Temp:%hi", actualHour, actualMinute, actualSecond, temperature);
  client.publish("temp_piec", msg);
}

void actualTime() {
  dateTime = NTPch.getNTPtime(1.0, 1);  // first parameter: Time zone in floating point (for India)
                                        // second parameter: 1 for European summer time, 2 for US daylight saving time; 0 for no DST adjustment
  
  if (dateTime.valid) {                 // check dateTime.valid before using the returned time
    NTPch.printDateTime(dateTime);
    actualHour = dateTime.hour;
    actualMinute = dateTime.minute;
    actualSecond = dateTime.second;
  }
}

float getTemperature() {
  sensor.requestTemperatures();   // take the temperature value from the sensor
  getTemperatureTimer.restart();  // restart the sensor timer
  
  return sensor.getTempC(sensorAddress);
}

short int changeLimitTemperatures(short int temperature, short int bottomLimit, short int upperLimit) {
  if (positionEncoder < encoder.getPosition()) {
    positionEncoder = encoder.getPosition();
    temperature++;
    turnOnBacklightLCD();
    returnMainWindowTimer.restart();
  }
  else if (positionEncoder > encoder.getPosition()) {
    positionEncoder = encoder.getPosition();
    temperature--;
    turnOnBacklightLCD();
    returnMainWindowTimer.restart();
    }

  if (temperature >= upperLimit) {
    temperature = upperLimit;
  }
  
  if (temperature <= bottomLimit) {
    temperature = bottomLimit;
  }

  return temperature;
}
// state 0 - normal temperature screen, 1 - too low temperature screen, 2 - too high temperature screen,
// 3 - low temperature change screen, 4 - high temperature change screen
void setLCDWindow(short int current_temp, short int min_temp, short int max_temp) {
  switch(stateLCD) {
    case 0: {
      printTextOnLcd(current_temp, "NORMAL TEMP.");
      turnOffBacklightLCD(TURN_OFF_BACKLIGHT_TIME);
      if (current_temp <= min_temp) {
        stateLCD = 1;
        lcd.clear();
        break;
      }
      else if (current_temp >= max_temp) {
        stateLCD = 2;
        lcd.clear();
        break;
      }
      else if (stateButton == 2) {
        stateLCD = 3;
        stateButton = 0;
        returnMainWindowTimer.restart();
        returnMainWindowTimer.time(SECS(RETURN_MAIN_WINDOW_TIME));
        lcd.clear();
        break;
      }
      break;
    }
    case 1: {
      printTextOnLcd(current_temp, "TEMP. TOO LOW!");
      turnOffBacklightLCD(TURN_OFF_BACKLIGHT_TIME);
      if (current_temp > min_temp) {
        stateLCD = 0;
        lcd.clear();
        break;
      }
      else if (stateButton == 2) {
        stateLCD = 3;
        stateButton = 0;
        returnMainWindowTimer.restart();
        returnMainWindowTimer.time(SECS(RETURN_MAIN_WINDOW_TIME));
        lcd.clear();
        break;
      }
      break;
    }
    case 2: {
      printTextOnLcd(current_temp, "TEMP. TOO HIGH!");
      turnOnBacklightLCD();
      if (current_temp < max_temp) {
        stateLCD = 0;
        lcd.clear();
        break;
      }
      else if (stateButton == 2) {
        stateLCD = 3;
        stateButton = 0;
        returnMainWindowTimer.restart();
        returnMainWindowTimer.time(SECS(RETURN_MAIN_WINDOW_TIME));
        lcd.clear();
        break;
      }
      break;
    }
    case 3: {
      printTextOnLcd("SET MIN TEMP.", "MIN TEMP.: ", min_temp); 
      turnOffBacklightLCD(TURN_OFF_BACKLIGHT_TIME);
      if (returnMainWindowTimer.available() == true && !(current_temp <= min_temp) && !(current_temp >= max_temp)) {
        stateLCD = 0;
        lcd.clear();
        break;
      }
      else if (returnMainWindowTimer.available() == true && current_temp <= min_temp) {
        stateLCD = 1;
        lcd.clear();
        break;
      }
      else if (returnMainWindowTimer.available() == true && current_temp >= max_temp) {
        stateLCD = 2;
        lcd.clear();
        break;
      }
      else if (stateButton == 2 && returnMainWindowTimer.available() == false) {
        stateLCD = 4;
        stateButton = 0;
        returnMainWindowTimer.restart();
        returnMainWindowTimer.time(SECS(RETURN_MAIN_WINDOW_TIME));
        lcd.clear();
        break;
      }
      break;
    }
    case 4: {
      printTextOnLcd("SET MAX TEMP.", "MAX TEMP.: ", max_temp);
      turnOffBacklightLCD(TURN_OFF_BACKLIGHT_TIME);
      if ((stateButton == 2 || returnMainWindowTimer.available() == true) && !(current_temp <= min_temp) && !(current_temp >= max_temp)) {
        stateLCD = 0;
        stateButton = 0;
        lcd.clear();
        break;
      }
      if ((stateButton == 2 || returnMainWindowTimer.available() == true) && current_temp <= min_temp) {
        stateLCD = 1;
        stateButton = 0;
        lcd.clear();
        break;
      }
      if ((stateButton == 2 || returnMainWindowTimer.available() == true) && current_temp >= MAX_TEMPERATURE) {
        stateLCD = 2;
        stateButton = 0;
        lcd.clear();
        break;
      }
      break;
    }
  }
}

void printTextOnLcd(int temperature, String textRow2) {
  lcd.setCursor(0, 0);
  lcd.print("*  TEMP: ");
  lcd.setCursor(9, 0);
  lcd.print(temperature);
  lcd.setCursor(11, 0);
  lcd.print(char(0));
  lcd.setCursor(12, 0);
  lcd.print("C");
  lcd.setCursor(15,0);
  lcd.print("*");
  lcd.setCursor(0, 1);
  lcd.print(textRow2);
  refreshLCDTimer.restart();
}

void printTextOnLcd(String textRow1, String textRow2, int thresholdTemperature) {
  lcd.setCursor(0, 0);
  if (stateLCD == 3) {
    lcd.print(char(1));
    lcd.setCursor(2, 0);
  }
  if (stateLCD == 4) {
    lcd.print(char(2));
    lcd.setCursor(2, 0);
  }
  lcd.print(textRow1);
  lcd.setCursor(0, 1);
  lcd.print(textRow2);
  lcd.setCursor(11, 1);
  lcd.print(thresholdTemperature);
  lcd.setCursor(13, 1);
  lcd.print(char(0));
  lcd.setCursor(14, 1);
  lcd.print("C");
  refreshLCDTimer.restart();
}

void turnOffBacklightLCD(int seconds) {
  backlightLCDTimer.time(SECS(seconds));
  if (backlightLCDTimer.available() == true) {
    lcd.noBacklight();
  }
}

void turnOnBacklightLCD() {
  lcd.backlight();
  backlightLCDTimer.restart();
}
// state 0 - no alarm sound, state - 1 alarm sound for low temperature, 2 - alarm sound for high temperature,
// 3 - no sound after switching off the alarm with the button
void setAlarmSound(short int current_temp, short int min_temp, short int max_temp, int warningInterval, int warningFreaquency, int alarmInterval, int alarmFreaquency) {
  switch(stateAlarm) {
    case 0: {
      stopAlarmSound();
      if (current_temp <= min_temp) {
        stateAlarm = 1;
        warningSoundDurationTimer.restart();
        break;
      }
      else if (current_temp >= max_temp) {
        stateAlarm = 2;
        break;
      }
      break;
    }
    case 1: {
      if (actualHour > 6 && actualHour < 22) {
        playAlarmSound(warningInterval, warningFreaquency);
      }
      if (current_temp > min_temp && current_temp < max_temp) {
        stateAlarm = 0;
        break;
      }
      else if (current_temp <= min_temp && (stateButton == 1 || warningSoundDurationTimer.available() == true)) {
        stateAlarm = 3;
        stateButton = 0;
        break;
      }
      break;
    }
    case 2: {
      playAlarmSound(alarmInterval, alarmFreaquency);
      if (current_temp < max_temp && current_temp > min_temp) {
        stateAlarm = 0;
        break;
      }
      else if (current_temp >= max_temp && stateButton == 1) {
        stateAlarm = 3;
        stateButton = 0;
        break;
      }
      break;
    }
    case 3: {
      stopAlarmSound();
      if (current_temp > min_temp && current_temp < max_temp) {
        stateAlarm = 0;
        break;
      }
      break;
    }
  }
}

void playAlarmSound(int interval, int frequency) {
  buzzerTimer.time(interval);
  if (buzzerTimer.available() == true) {
    playSound = !playSound;
    buzzerTimer.restart();
  }

  if (playSound == true) {
    digitalWrite(BUZZER_PIN, HIGH);
  }
  else {
    digitalWrite(BUZZER_PIN, LOW);
  }
}

void stopAlarmSound() {
  digitalWrite(BUZZER_PIN, LOW);
}

void myClickFunction() {
  stateButton = 1;  // one means the button was pressed once
  turnOnBacklightLCD();
}

void myDoubleClickFunction() {
  stateButton = 2;  // two means the button was pressed once
  turnOnBacklightLCD();
}

ICACHE_RAM_ATTR void encoderISR() {
  encoder.readAB();
}
/* ##################################### */